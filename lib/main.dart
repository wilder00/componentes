// awesomw snipet: mateap
import 'package:componentes/src/pages/routes/routes.dart';
import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:componentes/src/pages/alert_page.dart';

/* import 'package:componentes/src/pages/home_temp.dart'; */

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componentes APP',
      debugShowCheckedModeBanner: false,

      //para la configuración de leguajes
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('es', 'LA'),
      ],

      /* home: HomePage(), */ //quitamos home para colocar initial Route y routes
      initialRoute: '/',
      routes: getApplicationRoutes(),
      //para redireccionar rutas que no están trazadas en routes
      onGenerateRoute: (RouteSettings settings) {
        print('Ruta llamada: ${settings.name}');
        return MaterialPageRoute(
            builder: (BuildContext context) => AlertPage());
      },
    );
  }
}
