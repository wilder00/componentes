//importamos dart:async para trabajar con el timer
import 'dart:async';

import 'package:flutter/material.dart';

class ListaPage extends StatefulWidget {
  @override
  _ListaPageState createState() => _ListaPageState();
}

//las listas nos van a permitir trabajar con refresher, hacer infinite scroll,etc
class _ListaPageState extends State<ListaPage> {
  //creamos un scroll controles para tener una mejor configuración del scroll
  ScrollController _scrollController = new ScrollController();

  List<int> _listaNumeros = new List();
  int _ultimoItem = 0;

  bool _isLoading = false;

  //el init state es un método que no regresa nada, debe tener ese nombre y debe llamar al super que hace referencia al State<ListaPage>
  // se me viene a la mente de que se parece a los hooks de la fase de vida de um compononente
  @override
  void initState() {
    super.initState();

    _agregar10();

    //el listener dispara la funcion cuando el scroll tiene algún cambio
    _scrollController.addListener(() {
      //si la posición el scroll está en la posición máxima. Esto comprueba que esté al final de la página.
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        //_agregar10(); //quitamos el agregar10 porque usaremos futures
        fetchData();
      }
    });
  }

  //Se dispara cuando la página deja de existir en el stack de páginas
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    //para prevenir fugas de memoria, ya que si salimos de la página el controlador se queda y además se crea una más cada vez. Asi que si salimos de la página, eliminamos el controlador
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Listas"),
        ),
        // el stack es un widget como el Row o el Column, pero este lo que hace es apilar uno encima del otro, como sobrepuesto
        body: Stack(
          children: <Widget>[
            _crearLista(),
            _crearLoading(),
          ],
        ));
  }

  Widget _crearLista() {
    return RefreshIndicator(
      // en on Refresh, si se deja una función vacia el indicador de refresh seguirá cargando ya que no resolverá ningun Future [creo que se debe devolver algun Future]
      onRefresh: obtenerPagina1,
      // El builder es el método que se encargará de redibujar los elementos que están el la lista requerida. conforma son requeridos
      child: ListView.builder(
        //enlazamos el _scrollcontroller creado.
        controller: _scrollController,
        //responde a ¿cuántos elementos está contenida en estos momentos?
        itemCount: _listaNumeros.length,
        //itemBuilder es una función que recibe como parámetro el contextBuilder para saber dónde dibujar y el int que representa el indice de posición el la lista
        itemBuilder: (BuildContext context, int index) {
          final imagen = _listaNumeros[index];
          return FadeInImage(
            image: NetworkImage("https://picsum.photos/500/300/?image=$imagen"),
            placeholder: AssetImage("assets/jar-loading.gif"),
          );
        },
      ),
    );
  }

  Future<Null> obtenerPagina1() async {
    final duration = new Duration(seconds: 2);
    new Timer(duration, () {
      //limpiamos las listas de imagenes cargadas
      _listaNumeros.clear();
      _ultimoItem++;
      _agregar10();
    });

    return Future.delayed(duration);
  }

  void _agregar10() {
    for (var i = 1; i < 10; i++) {
      _ultimoItem++;
      _listaNumeros.add(_ultimoItem);
    }
    setState(() {});
  }

  Future<Null> fetchData() async {
    _isLoading = true;
    setState(() {});

    final duration = new Duration(seconds: 2);
    return new Timer(duration, respuestaHTTP);
  }

  void respuestaHTTP() {
    _isLoading = false;
    //con todo el animateTo estamos haciendo que cuando temine de cargar las imagenes se suba un poco el scroll indicanto que ya hay imagenes
    _scrollController.animateTo(
      _scrollController.position.pixels + 100,
      curve: Curves.fastOutSlowIn,
      duration: Duration(milliseconds: 250),
    );
    _agregar10();
  }

  Widget _crearLoading() {
    if (_isLoading) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
            ],
          ),
          SizedBox(
            height: 15.0,
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}
