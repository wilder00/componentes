// stless

import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/pages/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('componentes')),
      body: _lista(),
    );
  }

  Widget _lista() {
    //print(menuProvider.opciones);

    //hacerlo de esta manera haría que la app se viera como congelada si la petición al servidor demorara
    /* menuProvider.cargarData().then((opciones) {
      print("lista");
      print(opciones);
    }); */

    return FutureBuilder(
      future: menuProvider
          .cargarData(), //tiene que estar enlazada con lo que queremos esperar.
      initialData: [], //es la informacion que va a tener por defecto mientras no se ha resuelto el future. Es opcional
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        //los bilder son algo que permite dibujar en la pantalla
        return ListView(
          children: _listaItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];
    data.forEach((opt) {
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          //forma tradicional

          //Navigator.push(context, route);
          // el context es el build context que tenemos y sabe info global de la app
          //
          /* final route = MaterialPageRoute(builder: (context) {
            return AlertPage();
          });
          Navigator.push(context, route); */

          Navigator.pushNamed(context, opt['ruta']);
        },
      );
      opciones..add(widgetTemp)..add(Divider());
    });
    return opciones;
  }
}
