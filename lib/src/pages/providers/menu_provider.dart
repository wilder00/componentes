// será el que maneje la data que pusimos menu_opts

//para usar json se necesita importar[solo queremos usar rootBundle]:
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

class _MenuProvider {
  List<dynamic> opciones = [];

  _MenuProvider() {
    //cargarData(); //no se necesitará
  }

  // las funciones Asyn retornan Futures. Esto nos permite trabajar con un stateless widget con el Future Builder, es decir
  // que se construya cuando todo el proceso termina
  Future<List<dynamic>> cargarData() async {
    // este regresa un future
    final resp = await rootBundle.loadString('data/menu_opts.json');

    Map dataMap = json.decode(resp);
    print(dataMap['nombreApp']);
    opciones = dataMap['rutas'];

    return opciones;
  }
}

final menuProvider = new _MenuProvider();
