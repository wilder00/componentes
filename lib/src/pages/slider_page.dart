import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorSlider = 100;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sliders'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(children: <Widget>[
          _crearSlider(),
          _checkBox(),
          _crearSwitch(),
          //Ponemos la imagen dentrod e un Expanded para que este ocupe todo el espacio disponible
          Expanded(
            child: _crearImagen(),
          ),
        ]),
      ),
    );
  }

  Widget _crearSlider() {
    return Slider(
      //personalizando
      activeColor: Colors.indigoAccent,
      label: 'Tamaño de la imagen',
      //division divide todo el rango de min y max en el numero de partes indicados
      /* divisions: 20, */
      //se necesita de valores min y max
      value: _valorSlider,
      min: 10.0,
      max: 400.0,
      //para bloquear el slider lo hacemos con una condicional ternaria
      onChanged: (_bloquearCheck)
          ? null
          : (valor) {
              setState(() {
                _valorSlider = valor;
              });
            },
    );
  }

  Widget _crearImagen() {
    return Image(
      image: NetworkImage("https://clipground.com/images/vegeta-png-8.jpg"),
      width: _valorSlider,
      /* fit: BoxFit.contain, */
    );
  }

  Widget _checkBox() {
    //Checkbox básico
    /* return Checkbox(
      value: _bloquearCheck,
      onChanged: (valor) {
        setState(() {
          _bloquearCheck = valor;
        });
      },
    ); */

    //checkbox tipo list tile
    return CheckboxListTile(
      title: Text("Bloquear Slider"),
      value: _bloquearCheck,
      onChanged: (valor) {
        setState(() {
          _bloquearCheck = valor;
        });
      },
    );
  }

  _crearSwitch() {
    return SwitchListTile(
      title: Text("Bloquear Slider"),
      value: _bloquearCheck,
      onChanged: (valor) {
        setState(() {
          _bloquearCheck = valor;
        });
      },
    );
  }
}
