import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Alert Page'),
        ),
        body: Center(
          child: RaisedButton(
            child: Text("Mostrar Alerta"),
            color: Colors.blue,
            textColor: Colors.white,
            shape: StadiumBorder(),
            //ya que vamos a mandar una funcion con parámetro lo hacemos dentro de otra tipo flecha
            onPressed: () => _mostrarAlert(context),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.agriculture_outlined),
          onPressed: () {
            //tradicional: para regresar después de
            Navigator.pop(context);
          },
        ));
  }

  void _mostrarAlert(BuildContext context) {
    showDialog(
        // context es obligatorio
        context: context,
        // el barrierDismissible es para cerrar la alerta haciendo click fuera
        barrierDismissible: false,
        //builder es la funcion encargada de crear todo el dialog los botones
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            title: Text("Titulo"),
            content: Column(
              //hacemos que la clumna que por defecto ocupa todo el largo de la pantalla solo
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Este es el contenido de la caja de la alerta"),
                FlutterLogo(
                  size: 100.0,
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Cancelar"),
                //forma tradicional de salir del alert
                onPressed: () => Navigator.of(context).pop(),
              ),
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}
