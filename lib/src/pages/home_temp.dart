// snipet [crear un flutter statelesswidget]: stle
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = ['uno', 'Dos', 'tres', 'Cuatro', 'cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Componentes Temp 🥶"),
      ),
      body: ListView(
        /* children: _crearItems(), */
        children: _crearItemsCorta(),
      ),
    );
  }

  //primera forma:
  /* 
  List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();

    // in ocupa el elemento de la lista
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      lista.add(tempWidget);
      lista.add(Divider());
    }

    return lista;
  } */

  List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();

    // in ocupa el elemento de la lista
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      //utilizando el operador en cascada
      lista..add(tempWidget)..add(Divider());
    }

    return lista;
  }

  List<Widget> _crearItemsCorta() {
    var widgets = opciones.map((String item) {
      return Column(
        children: [
          ListTile(
            title: Text(item + "!"),
            subtitle: Text('Cualquier cosa 🤪'),
            leading: Icon(Icons.adb_sharp),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {},
          ),
          Divider()
        ],
      );
    }).toList(); //cambiamos a lista con toList

    return widgets;
  }
}
