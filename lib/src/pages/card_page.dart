import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cards"),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(
            height: 30.0,
          ),
          _cardTipo2()
        ],
      ),
    );
  }

  _cardTipo1() {
    return Card(
      //elevation hace referencia a la altura a la que estaría el card para producir una sombre, si es cero, no produce sombra
      elevation: 10,
      //para dar bordes a los cards
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue),
            title: Text("Soy el título de esta página"),
            subtitle: Text(
                "HOla soy el que esta escribiendoe todo lo que estas viendo en la pantalla que no vez"),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {},
              ),
              FlatButton(
                child: Text('Ok'),
                onPressed: () {},
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2() {
    //cambiamos la clase card por la clase Container para simular una card con un container y tener más control
    final card = Container(
      child: Column(
        children: [
          FadeInImage(
            image: NetworkImage(
                'https://free4kwallpapers.com/uploads/originals/2017/12/24/wonderfull-landscape-wallpaper.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            //poniendo la duración de aparición de la imagen para depués de que termine de cargar
            fadeInDuration: Duration(milliseconds: 200),
            height: 250.0,
            //para indicar a la imagen cómo debe ocupar la imagen que tiene
            fit: BoxFit.cover,
          ),
          /* Image( // utilizaremos el FadeInImage que es mejor que solo Image
            image: NetworkImage(
                "https://free4kwallpapers.com/uploads/originals/2017/12/24/wonderfull-landscape-wallpaper.jpg"),
          ), */
          Container(
              // el container es como un div de html
              padding: EdgeInsets.all(10.0),
              child: Text("No tengo idea de qué poner"))
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(boxShadow: <BoxShadow>[
        BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            //offset es la posicion como en un plano cartesiano
            offset: Offset(2.0, 10.0))
      ], borderRadius: BorderRadius.circular(30.0), color: Colors.white),
      // ClipRRect permite cortar todo lo que esté fuera el contenedor
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }
}
