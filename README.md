# componentes

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


# Notas:
---

## shotcuts snipet de awesome flutter snippets:

```mateapp``` nos permitirá crear el código base para una app en flutter.

### Completados sobre widgets:

Cuando demos click para que el cursos se mantenga sobre el nombre de un widgets, presionamos `ctrl` + `.`


---
## Componentes:

### ListView

Permite hacer scroll en el contenido que tenga internamente.
* ListView renderiza todo el elemento
* ListViewBuilder renderiza solo lo que se ve y a medida se acerque al contenido se va cargando

### ListTile:
Funciona como un widget elemento de un Listview 🤪 tiene como parámetros los siguientes widgets:
  * Title: el título
  * Subtitle: el subtítulo.
  * leading: Widgets que va al inicio del ListTile
  * trailing widgets que se corre hasta el final del ListTile
  * onTap: **Es un evento** que llama a la función que le asignemos. Se activa cuando tocamos el ListTile.

### FutureBuilder<T>:
Es un widget que permite dibujarse a si mismo basado en el último snapshot que interactuó con un future.
Un future tiene varios estados: cuando se pide info, cuando se resuelve y cuando da un error.    El FutureBuilder es un widget con una cierta logica adicional.


### Stack :
Es un widget que funciona similar a Column o a Row, pero la diferencia es que este apila sobreencimando como si tuvieran un z index.


### [Botones]:
#### - RaisedButton



---
## Sobre dart:

### operador en cascada:

```
List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();

    // in ocupa el elemento de la lista
    for (String opt in opciones) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      //utilizando el operador en cascada
      lista..add(tempWidget)..add(Divider());
      /* //equivalente
      lista.add(tempWidget);
      lista.add(Divider());
      */
    }

    return lista;
  }
```

### función map :
```
List<Widget> _crearItemsCorta() {
    var widgets = opciones.map((String item) {
      return ListTile(
        title: Text(item + "!"),
      );
    }).toList();//cambiamos a lista con toList
  }
```






---
## Sobre Markdown

### tablas: 

```
| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |
```

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |



---
## sobre git

### corregir un commit

```
git commit --amend
```
